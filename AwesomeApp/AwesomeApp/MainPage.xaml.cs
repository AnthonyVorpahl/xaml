﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AwesomeApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void MyButton_Clicked(object sender, EventArgs e)
        {
            if (Label.Text =="Hello World!")
            {
                Label.Text = "Mobile Dev Is Fun";

            }
            else
            {
                Label.Text = "Hello World!";
            }
        }
    }
}
