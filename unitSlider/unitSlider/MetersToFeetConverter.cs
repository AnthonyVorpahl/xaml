﻿using System;
using System.Globalization;
namespace unitSlider
{
    public class MetersToFeetConverter : Xamarin.Forms.IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double d)
            {
                return d * 3.28;
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double d)
            {
                return d / 3.28;
            }
            else
            {
                return value;
            }
        }
    }
}
