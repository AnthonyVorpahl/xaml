﻿using System;
using System.Globalization;
namespace unitSlider
{
    public class MetersToCentimetersConverter : Xamarin.Forms.IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double c)
            {
                return c * 100;
            }
            else
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
   
          if (value is double c)
            {
                return c / 100.0;
            }
            else
            {
                return value;
            }
        }
    }
}
