﻿using System;
using System.Globalization;
namespace unitSlider
{
    public class MetersToInchesConverter : Xamarin.Forms.IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double dd)
            {
                return dd * 39;
            }
            else
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double dd)
            {
                return dd / 39;
            }
            else
            {
                return value;
            }
        }
    }
}
