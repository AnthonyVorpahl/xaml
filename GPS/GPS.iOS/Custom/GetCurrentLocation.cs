﻿using System;
using GPS.Custom;
using Xamarin.Forms;
using Plugin.Geolocator;
using System.Threading.Tasks;

[assembly: Dependency(typeof(GPS.iOS.Custom.GetCurrentLocation))]
namespace GPS.iOS.Custom
{
    public class GetCurrentLocation : ICurrentLocation
    {
        public event EventHandler LocationUpdated;

        public GetCurrentLocation()
        {
        }



        public void UpdateCurrentLocation()
        {
            // Get GPS Location

            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 100;

            locator.GetPositionAsync(TimeSpan.FromSeconds(10)).ContinueWith(t =>
            {
                App.CurrentLat = t.Result.Latitude;
                App.CurrentLon = t.Result.Longitude;

                //Flag process as done
                LocationUpdated(null, null);

            }, TaskScheduler.FromCurrentSynchronizationContext());

            
        }
    }
}
