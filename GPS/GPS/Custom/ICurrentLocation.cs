﻿using System;
namespace GPS.Custom
{
    public interface ICurrentLocation
    {
        event EventHandler LocationUpdated;

        void UpdateCurrentLocation();
    }
}
