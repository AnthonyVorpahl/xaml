﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPS.Custom;
using Xamarin.Forms;

namespace GPS
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            DependencyService.Get<ICurrentLocation>().LocationUpdated += delegate
            {

                lblLat.Text = "Latitude " + App.CurrentLat.ToString();
                lblLong.Text = "Longitude " + App.CurrentLon.ToString();

            };
        }


        void btnUpdate_Clicked(object sender, System.EventArgs e)
        {
            DependencyService.Get<ICurrentLocation>().UpdateCurrentLocation();
        }

        void btnClear_Clicked(object sender, System.EventArgs e)
        {
            lblLat.Text = "Latitude";
            lblLong.Text = "Longitude";
        }
    }
}
