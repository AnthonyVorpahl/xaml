﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GPS
{
    public partial class App : Application
    {

        public static double CurrentLon;
        public static double CurrentLat;

        public App()
        {
            InitializeComponent();

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
