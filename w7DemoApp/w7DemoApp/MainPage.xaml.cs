﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace w7DemoApp
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        void HandleF2C_Clicked(object sender, EventArgs e)
        {
            
        }
        void HandleC2F_Clicked(object sender, EventArgs e)
        {
            CelciusToFahrenheitPage page = new CelciusToFahrenheitPage();
            Navigation.PushAsync(page);
        }
    }
}
