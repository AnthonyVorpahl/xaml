﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace w7DemoApp
{
    public partial class CelciusToFahrenheitPage : ContentPage
    {
        public CelciusToFahrenheitPage()
        {
            InitializeComponent();
        }
        const double math = 9 / 5.0;

        void HandleCalc_Clicked(object sender, EventArgs e)
        {
            string degreesCString = CelciusEntry.Text;

            double degreesC;
            if (double.TryParse(degreesCString, out degreesC))
            {
                double degreesF = 32 + (math * degreesC);
                FahrEntry.Text = degreesF.ToString();
            }
        }
    }
}
