﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace timersapp
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        TimeSpan updateInterval = TimeSpan.FromMilliseconds(10);
        bool isTimerRunning = false;
        TimeSpan timerOnUp;

        void Handle_StartButton_Click(object sender, EventArgs e)
        {
            Device.StartTimer(updateInterval, TimerOnUp);

        }

        void Handle_StopButton_Click(object sender, EventArgs e)
        {

        }

        void Handle_ClearButton_Click(object sender, EventArgs e)
        {

        }


    }
}
