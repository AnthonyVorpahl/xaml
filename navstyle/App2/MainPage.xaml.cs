﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void Goku_Clicked(object sender, EventArgs e)
        {
            gokupage page = new gokupage();
            Navigation.PushAsync(page);
        }

        private void Freeza_Clicked(object sender, EventArgs e)
        {
            Freeza page = new Freeza();
            Navigation.PushAsync(page);
        }
    }   

}
