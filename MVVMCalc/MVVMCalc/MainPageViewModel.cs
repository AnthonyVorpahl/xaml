﻿using System;
using System.ComponentModel;
namespace MVVMCalc
{
    public class MainPageViewModel : ViewModel
    {

        public MainPageViewModel()
        {
            AddCommand = new Xamarin.Forms.Command(Add);
            SubtractCommand = new Xamarin.Forms.Command(Subtract);
            MultiplyCommand = new Xamarin.Forms.Command(Multiply);
            DivideCommand = new Xamarin.Forms.Command(Divide, IsDivideCommandEnabled);

            ClearCommand = new Xamarin.Forms.Command(Clear);
        }

        bool IsDivideCommandEnabled()
        {
            if (Operand2 == 0.0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private double _operand1;
        private double _operand2;
        private double? _result;


        public double Operand1
        {
            get { return _operand1; }
            set
            {
                _operand1 = value;
                OnPropertyChanged();
            }
        }
        public double Operand2
        {
            get { return _operand2; }
            set
            {
                _operand2 = value;
                OnPropertyChanged();
                DivideCommand.ChangeCanExecute();
            }

        }
        public double? Result
        {
            get { return _result; }
            set
            {
                _result = value;
                OnPropertyChanged();
            }
        }


        public Xamarin.Forms.Command AddCommand { get; }
        public Xamarin.Forms.Command SubtractCommand { get; }
        public Xamarin.Forms.Command MultiplyCommand { get; }
        public Xamarin.Forms.Command DivideCommand { get; }
        public Xamarin.Forms.Command ClearCommand { get; }


        void Add()
        {
            Result = Operand1 + Operand2;
        }

        void Subtract()
        {
            Result = Operand1 - Operand2;
        }

        void Multiply()
        {
            Result = Operand1 * Operand2;
        }

        void Divide()
        {
            Result = Operand1 / Operand2;
        }

        void Clear()
        {
            Result = null;
            Operand1 = 0;
            Operand2 = 0;

        }

    }
}
