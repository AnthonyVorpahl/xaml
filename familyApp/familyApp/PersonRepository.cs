﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
namespace familyApp
{
    public class PersonRepository
    {
        public PersonRepository()
        {

            DbContextOptionsBuilder<FamilyContext> optionsBuilder = new DbContextOptionsBuilder<FamilyContext>();


            string dbFilename = "MyFamily.db";
            string dbDirectoryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string dbFilePath = System.IO.Path.Combine(dbDirectoryPath, dbFilename);


            string connectionString = $"Data Source={dbFilePath}";

            optionsBuilder.UseSqlite(connectionString);

            DbContextOptions<FamilyContext> options = optionsBuilder.Options;

            context = new FamilyContext(options);
            context.Database.EnsureCreated();
        }


        public FamilyContext context { get; }


        public void SaveNewPerson(persons p)
        {

            context.People.Add(p);


            context.SaveChanges();

        }

        public List<persons> GetAllPeople()
        {

            List<persons> allPeople = new List<persons>(context.People);

            return allPeople;

        }
    }
}
