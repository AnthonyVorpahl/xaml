﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace familyApp
{
    public class persons
    {
        [System.ComponentModel.DataAnnotations.Key]
        [Column("Person_ID")]
        public int Id { get; set; }
        [Column("First_Name")]
        public string FirstName { get; set; }
        [Column("Last_Name")]
        public string LastName { get; set; }
        [Column("Birth_date")]
        public DateTime BirthDate { get; set; }


        public string DisplayName
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
        }



    }
}
