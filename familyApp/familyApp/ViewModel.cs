﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace familyApp
{
    public class ViewModel
    {

        public ViewModel()
        {

            this.PersonRepository = new PersonRepository();

            AddCommand = new Command(Add);

            UpdateFamilyMembers();
        }

        public PersonRepository PersonRepository { get; }

        public ObservableCollection<persons> FamilyMembers { get; } = new ObservableCollection<persons>();

        public Command AddCommand { get; }
        public Xamarin.Forms.INavigation Navigation { get; set; }

        void Add()
        {
            AddFamilyMemberPage page = new AddFamilyMemberPage();

            SaveViewModel viewModel = new SaveViewModel();
            viewModel.Navigation = this.Navigation;

            page.BindingContext = viewModel;

            Navigation.PushAsync(page);


        }

        public void UpdateFamilyMembers()
        {
            this.FamilyMembers.Clear();

            foreach(var person in this.PersonRepository.GetAllPeople())
            {
                this.FamilyMembers.Add(person);
            }
        }

    }
}