﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
namespace familyApp
{
    public class SaveViewModel
    {

        public SaveViewModel()
        {
            this.PersonRepository = new PersonRepository();


            SaveCommand = new Command(Save);
        }
        public PersonRepository PersonRepository { get; }


        public persons FamilyMember { get; set; } = new persons();
        public Command SaveCommand { get; }

        public Xamarin.Forms.INavigation Navigation { get; set; }

        void Save()
        {
            this.PersonRepository.SaveNewPerson(this.FamilyMember);

            Navigation.PopAsync();
        }




    }
}
