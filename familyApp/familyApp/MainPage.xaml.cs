﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace familyApp
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        ViewModel modelView;
        public MainPage()
        {
            InitializeComponent();

            modelView = new ViewModel();
            this.BindingContext = modelView;
            modelView.Navigation = this.Navigation;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            modelView.UpdateFamilyMembers();
        }
    }
}
