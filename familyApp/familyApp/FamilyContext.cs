﻿using System;
using Microsoft.EntityFrameworkCore;
namespace familyApp
{
    public class FamilyContext : DbContext
    {
        public FamilyContext(DbContextOptions options) :base(options)
        {
        }

        public DbSet<persons> People { get; set; }

    }
}
