﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using movieapp.views;

namespace movieapp.Custom
{
    public class tablayout:TabbedPage
    {
        public tablayout()
        {
            Children.Add(new addPage() {Title = "Add Movie", IconImageSource="add" });
            Children.Add(new findPage() {Title = "Find Movie", IconImageSource="search" });

        }
    }
}
