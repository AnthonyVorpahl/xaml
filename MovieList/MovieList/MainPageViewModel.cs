﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
namespace MovieList
{
    public class MainPageViewModel
    {
        public MainPageViewModel()
        {
            this.MovieRepository = new MovieRepository();

            AddCommand = new Command(Add);

            UpdateMovieList();

        }

        public MovieRepository MovieRepository { get; }

        public ObservableCollection<Movie> MovieTitle { get; } = new ObservableCollection<Movie>();

        public Command AddCommand { get; }

        public INavigation Navigation { get; set; }

        public void Add()
        {

            AddMoviePage page = new AddMoviePage();

            AddMovieViewModel viewModel = new AddMovieViewModel();

            viewModel.Navigation = this.Navigation;



            page.BindingContext = viewModel;


            Navigation.PushAsync(page);
        }


        public void UpdateMovieList()
        {
            this.MovieTitle.Clear();

            foreach (var movie in this.MovieRepository.GetAllMovies())
            {
                this.MovieTitle.Add(movie);
            }
        }
    }
}

