﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
namespace MovieList
{
    public class MovieRepository
    {
        public MovieRepository()
        {
            DbContextOptionsBuilder<MovieContext> optionsBuilder = new DbContextOptionsBuilder<MovieContext>();


            string dbFileName = "MyMovies.db";
            string dbDirectoryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string dbFilePath = System.IO.Path.Combine(dbDirectoryPath, dbFileName);


            string connectionString = $"Data Source={dbFilePath}";


            optionsBuilder.UseSqlite(connectionString);


            DbContextOptions<MovieContext> options = optionsBuilder.Options;



            Context = new MovieContext(options);

            Context.Database.EnsureCreated();
        }

        public MovieContext Context { get; }


        public void SaveNewMovie(Movie m)
        {
            Context.movies.Add(m);

            Context.SaveChanges();
        }

        public List<Movie> GetAllMovies()
        {
            List<Movie> allMovies = new List<Movie>(Context.movies);

            return allMovies;
        }

        


    }
}
