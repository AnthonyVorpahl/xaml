﻿using System;
namespace MovieList
{
    public class Movie
    {

        [System.ComponentModel.DataAnnotations.Key]
        public int Id { get; set; }

        public string Title { get; set; }
        public string Rating { get; set; }

        public string DisplayMovie
        {
            get
            {
                return $"{Title} {Rating}";
            }
        }


    }
}
