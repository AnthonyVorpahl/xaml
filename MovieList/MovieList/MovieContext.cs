﻿using System;
using Microsoft.EntityFrameworkCore;
namespace MovieList
{
    public class MovieContext : DbContext
    {
        public MovieContext(DbContextOptions options) : base(options)
        {
        } 

        public DbSet<Movie> movies { get; set; }
    }
}
