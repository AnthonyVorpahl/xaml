﻿using System;
using Xamarin.Forms;
namespace MovieList
{
    public class AddMovieViewModel
    {
        public AddMovieViewModel()
        {
            this.MovieRepository = new MovieRepository();


            SaveCommand = new Command(Save);
        }

        public MovieRepository MovieRepository { get; }

        public Movie Movie { get; set; } = new Movie();

        public Command SaveCommand { get; }

        public INavigation Navigation { get; set; }


        void Save()
        {
            this.MovieRepository.SaveNewMovie(this.Movie);


            Navigation.PopAsync();

        }
    }
}
