﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace batman
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class batmanpage : TabbedPage
    {
        public batmanpage ()
        {
            InitializeComponent();
        }

        private void Detail_Clicked(object sender, EventArgs e)
        {
            Joker page = new Joker();
            Navigation.PushAsync(page);
        }
    }
}