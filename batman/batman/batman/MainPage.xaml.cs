﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace batman
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void Batman_Clicked(object sender, EventArgs e)
        {
            batmanpage page = new batmanpage();
            Navigation.PushAsync(page);
        }
        private void Joker_Clicked(object sender, EventArgs e)
        {
            Joker page = new Joker();
            Navigation.PushAsync(page);
        }

    }
}
