﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace batman
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Joker : MasterDetailPage
    {
        public Joker()
        {
            InitializeComponent();
          
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            this.IsPresented = !this.IsPresented;
        }
    }
}