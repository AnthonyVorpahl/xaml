﻿using System;
using System.ComponentModel;
namespace Application

{
	public class HelloWorldMVVMClass : System.ComponentModel.INotifyPropertyChanged
	{
		public HelloWorldMVVMClass()
		{
			ToggleMessageCommand = new Xamarin.Forms.Command(ToggleMessage);

		}
		public Xamarin.Forms.Command ToggleMessageCommand { get; }


		private string _message = "Hello World!";

		public string Message
		{
			get
			{
				return _message;
			}
			private set
			{
				_message = null;
				OnPropertyChanged("Message");

			}
		}


		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
				PropertyChangedEventArgs e = new PropertyChangedEventArgs(propertyName);
			this.OnPropertyChanged(this, e);
		}

		void ToggleMessage()
		{
			if (Message == "Hello World!")
			{
				Message = "GoodBye World!";
			}
			else
			{
				Message = "Hello World!";
			}
		}
	}
}