﻿using System;
using System.Collections.Generic;
namespace marathonApp
{
    public class GetRaceApiResult
    {
       public List<Race> races { get; set; }
    }
}
