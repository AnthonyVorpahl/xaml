﻿using System;
using System.Collections.Generic;
namespace marathonApp
{
    public class GetRaceResultsApiResult
    {
        public List<RaceResult> results { get; set; }
    }
}
