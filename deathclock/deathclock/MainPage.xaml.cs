﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace deathclock
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        TimeSpan updateInterval = TimeSpan.FromMilliseconds(10);
        bool isTimerRuning = false;
        TimeSpan timeToLive;
        
        void Handle_StartButton_click(object sender, EventArgs e)
        {

            if(!isTimerRuning)
            {


                double timeToliveSeconds;

                if (double.TryParse(TimeToLiveEntry.Text, out timeToliveSeconds))
                {
                    isTimerRuning = true;
                    StartButton.IsEnabled = false;

                    timeToLive = TimeSpan.FromSeconds(timeToliveSeconds);

                    //start the timer
                    Device.StartTimer(updateInterval, updateTimeToLive);
                }

            }


        }
        bool updateTimeToLive()
        {
            bool shouldThisMethodGetCalledAgainQuestionMark = true;

            if (isTimerRuning)
            {


                timeToLive = timeToLive - updateInterval;



                if (timeToLive < TimeSpan.Zero)
                {
                    timeToLive = TimeSpan.Zero;
                    shouldThisMethodGetCalledAgainQuestionMark = false;

                    // reset on zero

                    isTimerRuning = false;

                    DisplayAlert("Warning", "You are ded", "Thanks");
                }

                Device.BeginInvokeOnMainThread(UpdateTimeToLiveLabel);

            }
            

            return shouldThisMethodGetCalledAgainQuestionMark;
        }

        void UpdateTimeToLiveLabel()
        {
            TimeToLiveLabel.Text = timeToLive.ToString();


            StartButton.IsEnabled = timeToLive <= TimeSpan.Zero;
            this.ForceLayout();
        }

        void Handle_StopButton_Click(object sender, EventArgs e)
        {
            if (isTimerRuning)
            {
                isTimerRuning = false;
                StopButton.Text = "Resume";
            }
            else
            {
                isTimerRuning = true;
                StopButton.Text = "Stop";
            }
        }


    }
}
