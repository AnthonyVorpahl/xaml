﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace savestuff
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        void Save_Clicked(object sender, EventArgs e)
        {
            Application.Current.Properties["UserName"] = txtName.Text;
        }
        void Read_Clicked(object sender, EventArgs e)
        {
            if (Application.Current.Properties.ContainsKey("UserName"))
            {
                txtName.Text = Application.Current.Properties["UserName"].ToString();
            }
            else
            {
                DisplayAlert("Error", "Please Enter a Name", "Try Again");
            }
            
        }
        void Clear_Clicked(object sender, EventArgs e)
        {
            txtName.Text = string.Empty;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            Read_Clicked(null, null);
        }
    }
}
