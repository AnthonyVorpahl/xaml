﻿using dateNight.model;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace dateNight
{
    public partial class App : Application
    {
        public static DateCalc dateCalc;

        public App()
        {
            InitializeComponent();

            MainPage = new TabController();
            dateCalc = new DateCalc();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
