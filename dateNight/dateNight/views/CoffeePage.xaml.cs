﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace dateNight.views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CoffePage : ContentPage
    {
        public CoffePage()
        {
            InitializeComponent();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.dateCalc.CoffeeCost = txtCoffee.Text;


        }
    }
}