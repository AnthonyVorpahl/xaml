﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace dateNight.views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CostPage : ContentPage
    {
        public CostPage()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                txtPay.Text = App.dateCalc.GetTotalCost();
            }
            catch (Exception e)
            {
                DisplayAlert("Error", e.Message, "Try Again");
                txtPay.Text = "0.00";
            }
        }
    }
}