﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using dateNight.views;

namespace dateNight
{
    public class TabController : TabbedPage
    {

        public TabController()
        {
            Children.Add(new CoffePage() { IconImageSource = "coffeetab" });
            Children.Add(new DinnePage() { IconImageSource = "dinnertab"});
            Children.Add(new MoviePage() { IconImageSource = "movietab"});
            Children.Add(new CostPage() { IconImageSource = "paytab"});
        }


    }
}
